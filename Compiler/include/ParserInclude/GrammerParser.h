#ifndef GRAMMERPARSER_H
#define GRAMMERPARSER_H
#include <bits/stdc++.h>
#include <sstream>

using namespace std;

class GrammerParser
{
    public:
        GrammerParser();
        virtual ~GrammerParser();
        int getSymCount();
        int get_epsilon();
        int symbol_to_number(const string& symbol);
        string number_to_symbol(int number);
        struct NonTerminal{
            string symbol;
            vector<vector<int>> rule;
            vector< set<int> > mappedFirst;
            set<int> first;
            set<int> follow;
            vector<int> adjList;
        };
        vector<NonTerminal>* get_grammer();
        bool isterminal(int symbol);
        vector<NonTerminal> to_LL1();
        void read(string file_name);
        void print_map();
        void eliminate_left_recursion();
        void replace_rules(int i, int j, int c);
        void eliminate_immediate_left_recursion(int i);
        vector<string> gram;/*private*/
        vector<int> contain(int i, int j);/*private*/
        vector<NonTerminal> replaced;/*private*/
        void eliminate_left_factoring();
        void eliminate_factoring(int num, vector<int> y);
        bool cont(int num, vector<int> v);
        vector<int> get_vector(int num);


    protected:

    private:


        map<int, bool> terminal;
        vector<NonTerminal> grammer;
        map<string, int> string_to_number;
        map<int, string> number_to_string;
        map<string, int> string_to_number1;
        map<int, string> number_to_string1;
        int fill_maps_terminals(string g, int counter);
        void replace_string(string& subject, const string& search,
                          const string& replace);
        string remove_end_spaces(const string &s);
        vector<int> left_rec;
        vector<NonTerminal> added;
        vector<int> adjusted;
        vector<string> new_name;
        void replace_strings(string& subject, const string& search,
                          const string& replace);





};

#endif // GRAMMERPARSER_H
